# goldmark-wikilinks

[![GoDev][godev-image]][godev-url]

[godev-image]: https://pkg.go.dev/badge/github.com/TeddyDD/goldmark-wikilinks
[godev-url]: https://pkg.go.dev/github.com/TeddyDD/goldmark-wikilinks


goldmark-wikilinks is an extension for the [goldmark](http://github.com/yuin/goldmark) 
that allows you to use Wikimedia style links in Markdown documents.

## Usage

### Installation

```
go get go.teddydd.me/goldmark-wikilinks
```

### Markdown syntax

Example:

```
This is link for given [[topic]]

This link has [[alt text|topic]]

This is TiddlyWiki style image link [image[hello world]]
```

## License

MIT

## Author

Daniel Lewan
