package ast

import (
	"github.com/yuin/goldmark/ast"
)

type WikiLink struct {
	ast.BaseInline

	// Destination is a destination of this link.
	Destination []byte

	// Title is a title of this link.
	Title []byte
}

// Dump implements Node.Dump.
func (n *WikiLink) Dump(source []byte, level int) {
	m := map[string]string{
		"Destination": string(n.Destination),
		"Title":       string(n.Title),
	}
	ast.DumpHelper(n, source, level, m, nil)
}

var KindWikiLink = ast.NewNodeKind("Wikilink")

// Kind implements Node.Kind
func (w *WikiLink) Kind() ast.NodeKind {
	return KindWikiLink
}

func NewWikiLink(alt, destination []byte) *WikiLink {
	return &WikiLink{
		Destination: destination,
		Title:       alt,
	}
}
