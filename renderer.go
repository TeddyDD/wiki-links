package wikilinks

import (
	"fmt"

	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/renderer"
	"github.com/yuin/goldmark/renderer/html"
	"github.com/yuin/goldmark/util"
	mast "go.teddydd.me/goldmark-wikilinks/ast"
)

type RenderingMethod int

const (
	Link RenderingMethod = iota
	Func
)

type RenderFunc func(util.BufWriter, ast.Node)

type RendererConfig struct {
	html.Config

	Method     RenderingMethod
	RenderFunc RenderFunc
}

// SetOption implements renderer.SetOptioner.
func (c *RendererConfig) SetOption(name renderer.OptionName, value interface{}) {
	switch name {
	case optRenderingMethod:
		c.Method = value.(RenderingMethod)
	case optRenderFunc:
		c.RenderFunc = value.(RenderFunc)
	default:
		c.Config.SetOption(name, value)
	}
}

// A RendererOption interface sets options for the emoji renderer.
type RendererOption interface {
	renderer.Option

	SetRendererOption(*RendererConfig)
}

// options

type withRenderingMethod struct {
	value RenderingMethod
}

func (o *withRenderingMethod) SetConfig(c *renderer.Config) {
	c.Options[optRenderingMethod] = o.value
}

func (o *withRenderingMethod) SetRendererOption(c *RendererConfig) {
	c.Method = o.value
}

const optRenderingMethod renderer.OptionName = "WikilinksRenderingMethod"

type withRenderFunc struct {
	value RenderFunc
}

func WithRenderFunc(a RenderFunc) RendererOption {
	return &withRenderFunc{a}
}

func (o *withRenderFunc) SetConfig(c *renderer.Config) {
	c.Options[optRenderFunc] = o.value
}

func (o *withRenderFunc) SetRendererOption(c *RendererConfig) {
	c.RenderFunc = o.value
}

const optRenderFunc renderer.OptionName = "WikilinksRenderFunc"

// WithRenderingMethod is a functional option that indicates how links are rendered.
func WithRenderingMethod(a RenderingMethod) RendererOption {
	return &withRenderingMethod{a}
}

// renderer

type wikilinksRenderer struct {
	RendererConfig
}

func NewRenderer(opts ...RendererOption) renderer.NodeRenderer {
	r := &wikilinksRenderer{
		RendererConfig: RendererConfig{
			Config: html.NewConfig(),
			Method: Link,
		},
	}

	for _, opt := range opts {
		opt.SetRendererOption(&r.RendererConfig)
	}
	return r
}

// RendererFuncs registers NodeRendererFuncs to given NodeRendererFuncRegisterer.
func (r wikilinksRenderer) RegisterFuncs(reg renderer.NodeRendererFuncRegisterer) {
	reg.Register(mast.KindWikiLink, r.renderWikilink)
}

func (r *wikilinksRenderer) defaultRenderFunc(w util.BufWriter, n ast.Node) {
	node := n.(*mast.WikiLink)
	fmt.Fprintf(w, `<a href="%s">%s</a>`, node.Destination, node.Title)
}

func (r *wikilinksRenderer) renderWikilink(w util.BufWriter, source []byte, n ast.Node, entering bool) (ast.WalkStatus, error) {
	if !entering {
		return ast.WalkContinue, nil
	}

	switch r.Method {
	case Link:
		r.defaultRenderFunc(w, n)
	case Func:
		r.RenderFunc(w, n)
	default:
		panic("not implemented rendering method")
	}

	return ast.WalkContinue, nil
}
