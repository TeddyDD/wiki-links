package wikilinks

import (
	"fmt"
	"testing"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/testutil"
	"github.com/yuin/goldmark/util"
	mast "go.teddydd.me/goldmark-wikilinks/ast"
)

func TestLinkify(t *testing.T) {
	markdown := goldmark.New(
		goldmark.WithExtensions(
			Wikilinks,
		),
	)
	testutil.DoTestCaseFile(markdown, "_test/wikilinks.txt", t, testutil.ParseCliCaseArg()...)
}

func TestCustomRendering(t *testing.T) {
	markdown := goldmark.New(
		goldmark.WithRendererOptions(
			WithRenderingMethod(Func),
			WithRenderFunc(func(w util.BufWriter, n ast.Node) {
				node := n.(*mast.WikiLink)
				w.WriteString(fmt.Sprintf("%s -> %s", node.Title, node.Destination))
			}),
		),
		goldmark.WithExtensions(
			Wikilinks,
		),
	)
	testutil.DoTestCaseFile(markdown, "_test/wikilinks-custom-render.txt", t, testutil.ParseCliCaseArg()...)
}
