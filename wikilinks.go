package wikilinks

import (
	"bytes"

	mast "go.teddydd.me/goldmark-wikilinks/ast"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer"
	"github.com/yuin/goldmark/text"
	"github.com/yuin/goldmark/util"
)

type AltTextOrder int

const (
	FirstAltText AltTextOrder = iota
	FirstSource
)

const (
	optWikilinksAltTextOrder parser.OptionName = "WikilinksAltTextOrder"
	optWikilinksTrimSpaces   parser.OptionName = "WikilinksTrimSpaces"
)

// ParserConfig is a data structure that holds configuration of Wikilinks extension
type WikilinksConfig struct {
	AltTextOrder AltTextOrder
	TrimSpaces   bool
}

// SetOption implements SetOptioner.
func (w *WikilinksConfig) SetOption(name parser.OptionName, value interface{}) {
	switch name {
	case optWikilinksAltTextOrder:
		w.AltTextOrder = value.(AltTextOrder)
	case optWikilinksTrimSpaces:
		w.TrimSpaces = value.(bool)
	}
}

var _ parser.SetOptioner = &WikilinksConfig{}

type wikilinksParser struct {
	WikilinksConfig
}

func NewParser(opts ...ParserOption) parser.InlineParser {
	p := &wikilinksParser{
		WikilinksConfig: WikilinksConfig{
			AltTextOrder: FirstAltText,
			TrimSpaces:   true,
		},
	}

	for _, o := range opts {
		o.SetWikilinksOption(&p.WikilinksConfig)
	}
	return p
}

// Trigger returns a list of characters that triggers Parse method of
// this parser.
// Trigger characters must be a punctuation or a halfspace.
// Halfspaces triggers this parser when character is any spaces characters or
// a head of line
func (p *wikilinksParser) Trigger() []byte {
	return []byte{'['}
}

var (
	openSeq  = []byte("[[")
	closeSeq = []byte("]]")
	pipe     = []byte{'|'}
)

// Parse parse the given block into an inline node.
//
// Parse can parse beyond the current line.
// If Parse has been able to parse the current line, it must advance a reader
// position by consumed byte length.
func (p *wikilinksParser) Parse(parent ast.Node, block text.Reader, pc parser.Context) ast.Node {
	line, _ := block.PeekLine()
	if len(line) < 2 {
		return nil
	}

	if !bytes.HasPrefix(line, openSeq) {
		return nil
	}

	start := 2

	diff := bytes.Index(line[start:], closeSeq)
	if diff < 0 {
		return nil
	}

	if len(line) < start+diff+2 {
		return nil
	}

	end := start + diff
	if len(line) < end+2 {
		return nil
	}

	if len(line[start:end]) == 0 {
		return nil
	}

	block.Advance(end + 2)

	return p.nodeFrom(line[start:end])
}

func (p *wikilinksParser) nodeFrom(b []byte) ast.Node {
	hasAlt := false
	if bytes.Contains(b, pipe) {
		hasAlt = true
	}

	parts := bytes.SplitN(b, pipe, 2)

	if len(parts) < 1 {
		return nil
	}

	if p.TrimSpaces {
		parts[0] = bytes.TrimSpace(parts[0])
		if hasAlt {
			parts[1] = bytes.TrimSpace(parts[1])
		}
	}

	if !hasAlt {
		parts = append(parts, parts[0])
	}

	switch p.AltTextOrder {
	case FirstAltText:
		return mast.NewWikiLink(parts[0], parts[1])
	case FirstSource:
		return mast.NewWikiLink(parts[1], parts[0])
	default:
		panic("unknown AltTextOrder in Wikilinks")
	}
}

// A ParserOption interface sets options for the ParserOption.
type ParserOption interface {
	parser.Option
	SetWikilinksOption(*WikilinksConfig)
}

// options

type withAltTextOrder struct {
	value AltTextOrder
}

func (o *withAltTextOrder) SetParserOption(c *parser.Config) {
	c.Options[optWikilinksAltTextOrder] = o.value
}

func (o *withAltTextOrder) SetWikilinksOption(c *WikilinksConfig) {
	c.AltTextOrder = o.value
}

func WithAltTextOrder(value AltTextOrder) ParserOption {
	return &withAltTextOrder{
		value: value,
	}
}

type withTrimSpaces struct {
	value bool
}

func (o *withTrimSpaces) SetParserOption(c *parser.Config) {
	c.Options[optWikilinksTrimSpaces] = o.value
}

func (o *withTrimSpaces) SetWikilinksOption(c *WikilinksConfig) {
	c.TrimSpaces = o.value
}

func WithTrimSpaces(value bool) ParserOption {
	return &withTrimSpaces{
		value: value,
	}
}

// extension

type wikilinks struct {
	options []ParserOption
}

// Extend extends the Markdown.
func (e *wikilinks) Extend(m goldmark.Markdown) {
	pOpts := []ParserOption{}
	rOpts := []RendererOption{}
	for _, o := range e.options {
		if po, ok := o.(ParserOption); ok {
			pOpts = append(pOpts, po)
			continue
		}
		if ro, ok := o.(RendererOption); ok {
			rOpts = append(rOpts, ro)
		}
	}
	m.Parser().AddOptions(
		parser.WithInlineParsers(
			util.Prioritized(NewParser(pOpts...), 101),
		),
	)

	m.Renderer().AddOptions(
		renderer.WithNodeRenderers(
			util.Prioritized(NewRenderer(rOpts...), 999),
		),
	)
}

var Wikilinks = &wikilinks{}
